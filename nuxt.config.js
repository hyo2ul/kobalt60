export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'KOBALT60 | DIGITAL CREATIVE STUDIO',
    htmlAttrs: {
      lang: 'ko',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: '코발트식스티는 매순간 새로운 가능성을 만들어 가고 있습니다',
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content:
          'kobalt60, 코발트식스티, 디자인스튜디오, 방동욱, 그래픽디자인, 웹에이전시, 웹사이트, 디지털디자인',
      },
      { hid: 'og:image', property: 'og:image', content: '/img_og.jpg' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],
  styleResources: {
    scss: ['@/assets/scss/mixin.scss'],
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {
      src: '@/plugins/plugin.js',
      ssr: false,
    },
    {
      src: '@/plugins/mixin.js',
    },
    {
      src: '@/plugins/vue-awesome-swiper.js',
      mode: 'client',
    },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    '@nuxtjs/style-resources',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ['gsap'],
  },
  server: {
    host: '0',
  },
}
