import Vue from 'vue'
import anime from 'animejs/lib/anime.es.js'
import axios from 'axios'
import { gsap } from 'gsap'

Vue.prototype.$anime = anime
Vue.prototype.$axios = axios
export default (context, inject) => {
  inject('gsap', gsap)
}
// if (process.client) {
//   gsap.registerPlugin(MorphSVGPlugin);
// }
