import Vue from 'vue'
Vue.mixin({
  data() {
    return {
      routesData: [
        {
          name: 'about',
          to: '/About',
        },
        {
          name: 'works',
          to: '/Works',
        },
        {
          name: 'contact',
          to: '/Contact',
        },
      ],
      params: '',
      isMobile: false,
    }
  },
  beforeDestroy() {
    if (typeof window === 'undefined') return
    window.removeEventListener('resize', this.onResize, {
      passive: true,
    })
  },
  created() {
    this.params = this.$route.params.detail
  },
  mounted() {
    const html = document.documentElement
    // bugfix ios,
    let vh = 0
    const windowWidth = window.innerWidth
    vh = window.innerHeight * 0.01
    html.style.setProperty('--vh', `${vh}px`)
    html.style.setProperty('--windowWidth', `${windowWidth}px`)
    // resize
    this.onResize()
    // window.addEventListener('resize', this.onResize, {
    //   passive: true,
    // })

    // page move
    // window.scrollTo(0, 0)
  },
  destroyed() {},
  methods: {
    hasHistory() {
      return window.history.length > 2
    },
    goBack() {
      return this.hasHistory() ? this.$router.go(-1) : this.$router.push('/')
    },
    onResize() {
      this.isMobile = window.innerWidth < 1025
    },
  },
})

Vue.directive('scroll', {
  inserted(el, binding) {
    const f = (evt) => {
      if (binding.value(evt, el)) {
        el.removeEventListener('scroll', f)
      }
    }
    el.addEventListener('scroll', f)
  },
})
